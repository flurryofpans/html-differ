#OVERVIEW

This tool finds the closest related element from an Id from an HTML file in another HTML file

You can find the generated jar inside `target/sp-proj-1.0-spring-boot.jar` afterwards

Or you can use the already built jar inside `/bin`

###USAGE

```
java -jar bin/sp-proj-1.0-spring-boot.jar src/main/resources/origin.html src/main/resources/diff2.html
```

You can also user `URL`s like so:

```
java -jar bin/sp-proj-1.0-spring-boot.jar https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-0-origin.html https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-2-container-and-clone.html
```

###YOU CAN PROVIDE AN ID AFTER BOTH FILES

```
java -jar bin/sp-proj-1.0-spring-boot.jar src/main/resources/origin.html src/main/resources/diff3.html <id>
```

###BUILD AND TESTS

```
mvn install
```

