package julian;

import julian.differ.HTMLDiffer;
import julian.differ.model.Found;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

import java.io.File;

@SpringBootApplication
public class HTMLDifferApp implements CommandLineRunner {
    private static Logger LOGGER = LoggerFactory.getLogger(HTMLDifferApp.class);

    @Autowired
    HTMLDiffer differ;

    public static void main(String[] args)  {
        new SpringApplicationBuilder(HTMLDifferApp.class)
                .web(WebApplicationType.NONE)
                .bannerMode(Banner.Mode.OFF)
                .run(args);
    }

    public void run(String... args) throws Exception {
        if(args.length == 3)
            differ.setId(args[2]);

        LOGGER.info("Using " + args[0] + " as origin");
        LOGGER.info("To find button in " + args[1]);

        Found okElement = differ.findOkElement(args[0], args[1]);

        LOGGER.info("Found element: " + okElement.getElement());
        LOGGER.info("Found because: " + okElement.getWhy());
    }
}
