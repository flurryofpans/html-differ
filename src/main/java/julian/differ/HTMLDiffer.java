package julian.differ;

import julian.differ.model.Found;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Objects.isNull;
import static java.util.Spliterators.spliteratorUnknownSize;
import static java.util.stream.Collectors.toList;
import static java.util.stream.StreamSupport.stream;

@Service
public class HTMLDiffer {
    private String id = "make-everything-ok-button";

    public void setId(String id) {
        this.id = id;
    }

    public Found findOkElement(String origin, String diff) throws IOException {
        Document originDoc = getDocument(origin);
        Document diffDoc = getDocument(diff);

        Element ok = originDoc.getElementById(id);

        return doFindOk(ok, diffDoc.getAllElements());
    }

    private Found doFindOk(Element ok, Elements elementsByTag) {
        List<Element> textMatches = textMatches(ok, elementsByTag);

        if (textMatches.size() == 1)
            return Found.element(textMatches.get(0), "HTML text inside the tag matches");

        return findBestMatch(
                mostCommonAttrs(ok, textMatches),
                mostCommonAttrs(ok, streamElements(elementsByTag).collect(toList())));
    }

    private Found findBestMatch(Found textAndAttrs, Found justAttrs) {
        Found bestMatch = Stream.of(textAndAttrs, justAttrs)
                .filter(finding -> !isNull(finding))
                .max(Comparator.comparing(Found::getTimes))
                .get();

        if(bestMatch.equals(textAndAttrs))
            return bestMatch.alsoBecause("text matches perfectly");
        else
            return justAttrs;
    }

    private List<Element> textMatches(Element ok, Elements elementsByTag) {
        return streamElements(elementsByTag)
                .filter(el -> el.text().equals(ok.text()))
                .collect(toList());
    }

    private Found mostCommonAttrs(Element ok, List<Element> elementsByTag) {
        Integer max = 0;
        Element found = null;

        for (Element element : elementsByTag) {
            Integer thisComparison = 0;

            for (Attribute okAttr : ok.attributes()) {
                Boolean isAttributeMatch = element.attributes().get(okAttr.getKey()).equals(okAttr.getValue());

                if (isAttributeMatch) thisComparison += 1;
            }

            if (thisComparison > max) {
                max = thisComparison;
                found = element;
            }
        }

        return Found.element(found, "Attributes coincidence, times: [ " + max + " ]");
    }

    private Document getDocument(String uri) throws IOException {
        return ResourceUtils.isUrl(uri)
                ? Jsoup.connect(uri).get()
                : Jsoup.parse(Paths.get(uri).toFile(), "UTF-8");
    }

    private Stream<Element> streamElements(Elements elements) {
        return stream(spliteratorUnknownSize(elements.iterator(), 0), false);
    }

}
