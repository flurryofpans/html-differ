package julian.differ.model;

import org.jsoup.nodes.Element;

import java.util.Optional;

import static java.util.Objects.isNull;

public class Found {
    String element;
    String why;

    public Found(String element, String why) {
        this.element = element;
        this.why = why;
    }

    public static Found element(Element match, String why) {
        return isNull(match) ? null : new Found(match.toString(), why);
    }

    public String getElement() {
        return element;
    }

    public String getWhy() {
        return why;
    }

    public Integer getTimes() {
        String times = why;

        times = times.substring(times.indexOf("[ ") + 1);
        times = times.substring(0, times.indexOf(" ]"));

        return Integer.valueOf(times.trim());
    }

    public Found alsoBecause(String moreWhy) {
        why = why + " also " + moreWhy;

        return this;
    }
}
