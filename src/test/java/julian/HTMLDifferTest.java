package julian;

import julian.differ.HTMLDiffer;
import julian.differ.model.Found;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;

import static junit.framework.TestCase.assertEquals;

public class HTMLDifferTest {
    private HTMLDiffer differ = new HTMLDiffer();

    @Test
    public void canFindFirstDiff() throws IOException {
        Found ok = differ.findOkElement("src/main/resources/origin.html", "src/main/resources/diff1.html");

        assertEquals(ok.getWhy(), "HTML text inside the tag matches");
        assertEquals(ok.getElement(), "<a class=\"btn btn-success\" href=\"https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-1-evil-gemini.html#check-and-ok\" title=\"Make-Button\" rel=\"done\" onclick=\"javascript:window.okDone(); return false;\"> Make everything OK </a>");
    }

    @Test
    public void canFindSecondDiff() throws IOException {
        Found ok = differ.findOkElement("src/main/resources/origin.html", "src/main/resources/diff2.html");

        assertEquals(ok.getWhy(), "Attributes coincidence, times: [ 2 ] also text matches perfectly");
        assertEquals(ok.getElement(), "<a class=\"btn test-link-ok\" href=\"https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-2-container-and-clone.html#ok\" title=\"Make-Button\" rel=\"next\" onclick=\"javascript:window.okComplete(); return false;\"> Make everything OK </a>");
    }

    @Test
    public void canFindThirdDiff() throws IOException {
        Found ok = differ.findOkElement("src/main/resources/origin.html", "src/main/resources/diff3.html");

        assertEquals(ok.getWhy(), "Attributes coincidence, times: [ 3 ]");
        assertEquals(ok.getElement(), "<a class=\"btn btn-success\" href=\"https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-3-the-escape.html#ok\" title=\"Do-Link\" rel=\"next\" onclick=\"javascript:window.okDone(); return false;\"> Do anything perfect </a>");
    }

    @Test
    public void canFindFourthDiff() throws IOException {
        Found ok = differ.findOkElement("src/main/resources/origin.html", "src/main/resources/diff4.html");

        assertEquals(ok.getWhy(), "Attributes coincidence, times: [ 3 ]");
        assertEquals(ok.getElement(), "<a class=\"btn btn-success\" href=\"https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-4-the-mash.html#ok\" title=\"Make-Button\" rel=\"next\" onclick=\"javascript:window.okFinalize(); return false;\"> Do all GREAT </a>");
    }
}
